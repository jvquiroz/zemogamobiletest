# ZemogaMobileTest

This proyect consist on an App that lists all messages and their details from JSONPlaceholder. It is intended to be a practical test for ZEMOGA. 

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

Have a working installation of Android Studio

### Installing

1. Clone de repo
```sh
git clone git@bitbucket.org:jvquiroz/zemogamobiletest.git
```


2. Wait for the proyect to build


3. Run the app

## Built With

* [Retrofit2](https://square.github.io/retrofit/) - HTTP client for Android
* [Gradle](https://gradle.org/) - Dependency Management
* [AndroidX](https://developer.android.com/Android/Jetpack) - Improvement to the original Android Support Library.

## Authors

* **José Quiroz** - *Initial work* - [jvquiroz](https://bitbucket.org/jvquiroz)
