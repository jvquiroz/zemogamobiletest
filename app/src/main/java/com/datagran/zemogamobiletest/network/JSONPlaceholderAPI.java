package com.datagran.zemogamobiletest.network;

import com.datagran.zemogamobiletest.model.Comment;
import com.datagran.zemogamobiletest.model.Post;
import com.datagran.zemogamobiletest.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JSONPlaceholderAPI {

    @GET("/posts")
    Call<List<Post>> getAllPosts();

    @GET("/users")
    Call<List<User>> getUser(@Query("id") int userId);

    @GET("/comments")
    Call<List<Comment>> getComments(@Query("postId") int postId);

}
