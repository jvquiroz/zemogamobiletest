package com.datagran.zemogamobiletest.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceBuilder {

    private static final String BASE_URL = "https://jsonplaceholder.typicode.com";

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static <S> S build(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
