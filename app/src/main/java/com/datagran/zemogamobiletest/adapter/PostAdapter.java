package com.datagran.zemogamobiletest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.datagran.zemogamobiletest.R;
import com.datagran.zemogamobiletest.model.Post;
import com.datagran.zemogamobiletest.utils.SharedPreferencesUtil;
import com.datagran.zemogamobiletest.view.PostDetailActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {

    private final OnPostListener onPostListener;
    private List<Post> data;
    private Context context;

    public PostAdapter(Context context, OnPostListener onPostListener) {
        this.context = context;
        this.onPostListener = onPostListener;
    }

    public void setData(List<Post> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public List<Post> getData() {
        return data;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.post_item, parent, false);
        return new PostViewHolder(view, onPostListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        Post post = data.get(position);
        holder.title.setText(post.getTitle());
        if (isFav(post.getId())) {
            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite));
            holder.icon.setVisibility(View.VISIBLE);
        } else if (position < 20 && !isNew(post.getId())) {
            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.new_post));
            holder.icon.setVisibility(View.VISIBLE);
        } else {
            holder.icon.setVisibility(View.GONE);
        }
    }

    private boolean isFav(int postId) {
        return SharedPreferencesUtil.getBoolean(context, PostDetailActivity.FAV_KEY +  postId);
    }

    private boolean isNew(int postId) {
        return SharedPreferencesUtil.getBoolean(context, PostDetailActivity.POST_KEY +  postId);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void remove(int position) {
        if (position < getItemCount()) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title;
        ImageView icon;
        OnPostListener onPostListener;

        public PostViewHolder(@NonNull View itemView, OnPostListener onPostListener) {
            super(itemView);
            title = itemView.findViewById(R.id.title_post);
            icon = itemView.findViewById(R.id.icon_post);
            this.onPostListener = onPostListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onPostListener.onPostClick(data.get(getAdapterPosition()), getAdapterPosition());
        }
    }

    public interface OnPostListener {
        void onPostClick(Post post, int position);
    }
}
