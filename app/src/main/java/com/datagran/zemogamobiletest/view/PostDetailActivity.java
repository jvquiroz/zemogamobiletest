package com.datagran.zemogamobiletest.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.datagran.zemogamobiletest.R;
import com.datagran.zemogamobiletest.adapter.CommentAdapter;
import com.datagran.zemogamobiletest.model.Comment;
import com.datagran.zemogamobiletest.model.Post;
import com.datagran.zemogamobiletest.model.User;
import com.datagran.zemogamobiletest.network.JSONPlaceholderAPI;
import com.datagran.zemogamobiletest.network.ServiceBuilder;
import com.datagran.zemogamobiletest.utils.SharedPreferencesUtil;

import java.util.List;

public class PostDetailActivity extends BaseActivity {

    public static final String POST_KEY = "post";
    public static final String FAV_KEY = "fav";

    private Post post;
    private JSONPlaceholderAPI jsonPlaceholderAPI;
    private CommentAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        progressView = findViewById(R.id.progressContainer);
        configureCommentsRecyclerView();

        post = getIntent().getParcelableExtra(POST_KEY);
        invalidateOptionsMenu();
        SharedPreferencesUtil.setData(this, POST_KEY + post.getId(), true);

        if (post == null) {
            return;
        } else {
            TextView description = findViewById(R.id.description);
            description.setText(post.getBody());
        }

        jsonPlaceholderAPI = ServiceBuilder.build(JSONPlaceholderAPI.class);
        setLoadingState(true);
        getUserData(post.getUserId());
    }

    private void configureCommentsRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.comments);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isFav = SharedPreferencesUtil.getBoolean(this, FAV_KEY + post.getId());
        configureFavButton(menu.findItem(R.id.action_favorite), isFav);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                return toggleFavorite(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean toggleFavorite(MenuItem item) {
        boolean isFav = SharedPreferencesUtil.getBoolean(this, FAV_KEY + post.getId());
        SharedPreferencesUtil.setData(this, FAV_KEY + post.getId(), !isFav);
        configureFavButton(item, !isFav);
        return true;
    }

    private boolean configureFavButton(MenuItem item, boolean isFav) {
        item.setIcon(ContextCompat.getDrawable(this, isFav ? R.drawable.ic_favorite : R.drawable.ic_favorite_border));
        return isFav;
    }

    private void getUserData(int userId) {
        Call<List<User>> call = jsonPlaceholderAPI.getUser(userId);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.body().size() > 0) {
                    setUserData(response.body().get(0));
                }
                getComments(post.getId());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                setLoadingState(false);
                Toast.makeText(PostDetailActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getComments(int postId) {
        Call<List<Comment>> call = jsonPlaceholderAPI.getComments(postId);
        call.enqueue(new Callback<List<Comment>>() {

            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                adapter = new CommentAdapter(response.body());
                recyclerView.setAdapter(adapter);
                setLoadingState(false);
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                setLoadingState(false);
            }
        });
    }

    private void setUserData(User user) {
        TextView name = findViewById(R.id.name);
        TextView phone = findViewById(R.id.phone);
        TextView email = findViewById(R.id.email);
        TextView website = findViewById(R.id.website);

        name.setText(user.getName());
        phone.setText(user.getPhone());
        email.setText(user.getEmail());
        website.setText(user.getWebsite());
    }
}
