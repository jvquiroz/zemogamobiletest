package com.datagran.zemogamobiletest.view;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    protected View progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setLoadingState(boolean isLoading) {
        progressView.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
    }
}
