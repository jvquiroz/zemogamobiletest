package com.datagran.zemogamobiletest.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.Toast;

import com.datagran.zemogamobiletest.R;
import com.datagran.zemogamobiletest.adapter.PostAdapter;
import com.datagran.zemogamobiletest.model.Post;
import com.datagran.zemogamobiletest.network.JSONPlaceholderAPI;
import com.datagran.zemogamobiletest.network.ServiceBuilder;
import com.datagran.zemogamobiletest.utils.SharedPreferencesUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements PostAdapter.OnPostListener {


    private static final int VIEW_POST_DETAIL = 1;
    private JSONPlaceholderAPI jsonPlaceholderAPI;
    private PostAdapter adapter;
    private RecyclerView recyclerView;
    private int currentPostPosition = -1;
    private List<Post> posts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        progressView = findViewById(R.id.progressContainer);
        setUpPostRecyclerView();
        jsonPlaceholderAPI = ServiceBuilder.build(JSONPlaceholderAPI.class);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(getFavClickListener());
        getPosts();
    }

    private View.OnClickListener getFavClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(R.string.delete_dialog_message)
                       .setPositiveButton(getString(R.string.action_yes), new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                                deleteAllPost();
                           }
                       })
                       .setNegativeButton(getString(R.string.action_no), new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                                //no-op
                           }
                       })
                       .setTitle(R.string.delete_dialog_title)
                       .show();
            }
        };
    }

    private void deleteAllPost() {
        SharedPreferencesUtil.deleteAll(this);
        adapter.setData(new ArrayList<Post>());
    }

    private void setUpPostRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.posts);
        adapter = new PostAdapter(this, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        setUpItemTouchHelper();
    }

    private void getPosts() {
        setLoadingState(true);
        Call<List<Post>> call = jsonPlaceholderAPI.getAllPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                posts = response.body();
                adapter.setData(posts);
                setLoadingState(false);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                setLoadingState(false);
                Toast.makeText(MainActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPostClick(Post post, int position) {
        currentPostPosition = position;
        Intent intent = new Intent(this, PostDetailActivity.class);
        intent.putExtra(PostDetailActivity.POST_KEY, post);
        startActivityForResult(intent, VIEW_POST_DETAIL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIEW_POST_DETAIL) {
            adapter.notifyItemChanged(currentPostPosition);
            currentPostPosition = -1;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                getPosts();
                return true;
            case R.id.filter_all:
                item.setChecked(true);
                adapter.setData(posts);
                return true;
            case R.id.filter_fav:
                item.setChecked(true);
                filterFavPost();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void filterFavPost() {
        List<Post> favPosts = new ArrayList<>();
        for (Post post  : posts) {
            boolean isFav = SharedPreferencesUtil.getBoolean(this, PostDetailActivity.FAV_KEY +  post.getId());
            if (isFav) {
                favPosts.add(post);
            }
        }
        adapter.setData(favPosts);
    }

    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                    @NonNull RecyclerView.ViewHolder viewHolder,
                    @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                adapter.remove(position);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
    }
}
