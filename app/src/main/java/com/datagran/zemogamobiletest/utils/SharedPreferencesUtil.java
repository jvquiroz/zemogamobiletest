package com.datagran.zemogamobiletest.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtil {

    private static final String APP = "zemoga_mobile_test";

    static public boolean getBoolean(Context context, String key) {
        return context.getSharedPreferences(APP, Context.MODE_PRIVATE).getBoolean(key, false);
    }

    static public void setData(Context context, String key, boolean value) {
        context.getSharedPreferences(APP, Context.MODE_PRIVATE)
               .edit()
               .putBoolean(key, value)
               .apply();
    }

    static public void deleteAll(Context context) {
        context.getSharedPreferences(APP, Context.MODE_PRIVATE)
               .edit()
               .clear()
               .apply();
    }
}
